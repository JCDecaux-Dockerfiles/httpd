FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

RUN yum install supervisor httpd mod_ssl -y \
  && yum clean all

COPY supervisord.conf /etc/supervisord.conf

VOLUME ["/var/www/html"]

EXPOSE 80 443

CMD ["/usr/bin/supervisord"]
